/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var merchantName = "";
var storeType="";
var names = {
	"module": "Manufacturer",
	"partnerid": "Retailer"
};
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },

    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
		console.log('device ready');
		goHome();

        //var parentElement = document.getElementById(id);
        //var listeningElement = parentElement.querySelector('.listening');
        //var receivedElement = parentElement.querySelector('.received');

        //listeningElement.setAttribute('style', 'display:none;');
        //receivedElement.setAttribute('style', 'display:block;');
    }
};
uniLimit = 20;

goHome = function() {

	var appDiv = $(".appwrap");
	appDiv.empty();
	appDiv.append(" <div id=\"deviceready\" class=\"blink\"> " + 
				" <div class=\"mikeIsATerribleRapper\"> " +
					" <div storeType=\"module\" class=\"store header manu\">Manufacturer</div> " + 
					" <div storeType=\"partnerid\" class=\"store header reta\">Retailer</div> " + 
				" </div> " +
				" </div> " +
				" <div id=\"top10\" class=\"header\">Top trending products</div> "
				);
	$('.store').click(function(e) {listMerchants(e);});
	merchantName = "";
	storeType = "";
	$("#searchbutton").click(function(e) {
		e.preventDefault();
		listSearchResults($("#searchbox").val());
	});
	loadTopTrending();

	/*
	 * Don't need this - it's double catching.  weird
	$("#searchbox").bind("keypress", function(e) {
		e.preventDefault();
		var code = e.keyCode || e.which;
		if(code == 13) {
			console.log("yo");
			console.log($("#searchbox").val());
		}
	});
	*/
};

loadTopTrending = function(query) {
	var total = 20;

	var $topHolder = $('#top10');
	$.ajax({
		url: "https://insights-api.newrelic.com/v1/accounts/366519/query?nrql=SELECT%20uniqueCount(dsply_uuid),%20uniques(%60channel-product-name%60,%201),%20uniques(module)%20FROM%20Transaction%20where%20%60channel-product-name%60!%3D%27Undefined%27%20FACET%20wcpc%20since%201%20week%20ago%20limit%2020", 
		headers: {
			"Accept": "application/json",
			"X-Query-Key": "S6VfFMLd1igICiDdDuvVBpAF4pobxwVX"
		}})
		.done(function(data) {
			var maxCount = data.facets[0].results[0].uniqueCount;
			$.each(data.facets, function(i) {
				var wcpc = data.facets[i].name;
				var itemName = data.facets[i].results[1].members[0];
				var mname = data.facets[i].results[2].members[0];
				var count = data.facets[i].results[0].uniqueCount;
				var weighted = Math.round((count / maxCount)*100);
				$topHolder.append("<div itemname=\"" + itemName.toString().replace('"', '') + "\" merchantname=\"" + mname + "\" wcpc=\"" + wcpc +  "\" class=\"item clickable\"><span class=\"leftItem\">" +  notTooLong(itemName) + "</span><span class=\"rightItem\">" + weighted + "</span></div>");
			});
			$('.item').click(function(e) {showPP(e);});
		});
};

$("#home").click(function(e) {
	goHome();
});

fireSpinner = function() {
	$( "#ajax-loader" ).ajaxLoader( {
		lineWidth: 8,
		top: {
			color: "#4b98eb"
		},
		bottom: {
			color: "#377fcc"
		}
	});
};

killSpinner = function() {
	$('#ajax-loader').remove();
};

addSpinner = function(elem) {
	elem.append("<div id=\"ajax-loader\"></div>");
	fireSpinner();
};

listSearchResults = function(query) {
	var appDiv = $(".appwrap");
	appDiv.empty();
	var total = 30;
	var perstore = 5;

	appDiv.append("<div id=\"searchHolder\" class=\"header\">Results for <span class=\"query\">" + query + "</span></div>");
	addSpinner(appDiv);

	var $searchHolder = $('#searchHolder');
	$.ajax({
		url: "https://insights-api.newrelic.com/v1/accounts/366519/query?nrql=SELECT%20uniqueCount(dsply_uuid),%20uniques(wcpc,%20" + perstore + "),%20uniques(mpn,%20" + perstore + ")%20FROM%20Transaction%20where%20%60channel-product-name%60%20like%20%27%25" + query + "%25%27%20or%20mpn%20like%20%27%25" + query + "%25%27%20facet%20module%20since%201%20week%20ago", 
		headers: {
			"Accept": "application/json",
			"X-Query-Key": "S6VfFMLd1igICiDdDuvVBpAF4pobxwVX"
		}})
		.done(function(data) {
			killSpinner();
			var maxCount = data.facets[0].results[0].uniqueCount;
			$.each(data.facets, function(i) {
				if (total <= 0) {
					return false;
				}
				var mname = data.facets[i].name;

				/*
				var wcpc = sorted[i][0];
				var itemName = nameObj[wcpc];
				var weighted = Math.round((wcpcCounts[wcpc] / maxCount)*100);
				$header.append("<div wcpc=\"" + wcpc +  "\" class=\"item clickable\"><span class=\"leftItem\">" + notTooLong(nameObj[wcpc]) + "</span><span class=\"rightItem\">" + weighted + "</span></div>");
				*/


				var weighted = Math.round((data.facets[i].results[0].uniqueCount / maxCount)*100);
				$.each(data.facets[i].results[1].members, function(j) {
					$searchHolder.append("<div itemname=\"" + data.facets[i].results[2].members[j].toString().replace('"', '') + "\" merchantname=\"" + mname + "\" wcpc=\"" + data.facets[i].results[1].members[j] +  "\" class=\"item clickable\"><span class=\"leftItem\">" + notTooLong(data.facets[i].results[2].members[j]) + "</span><span class=\"rightItem\">" + weighted + "</span></div>");
					total = total - 1;
				});
			});
			$('.item').click(function(e) {showPP(e);});
		});
};

listMerchants = function(e) {
	storeType = $(e.target).attr("storeType");
	var appDiv = $(".appwrap");
	appDiv.empty();

	appDiv.append("<div id=\"merchantHolder\" class=\"header\">" + names[storeType] + "</div>");
	addSpinner(appDiv);

	var $merchantHolder = $('#merchantHolder');
	$.ajax({
		url: "https://insights-api.newrelic.com/v1/accounts/366519/query?nrql=SELECT%20count(*)%20FROM%20Transaction%20where%20profile%3D%27webcollage%27%20facet%20" + storeType + "%20limit%2020%20since%201%20week%20ago", 
		headers: {
			"Accept": "application/json",
			"X-Query-Key": "S6VfFMLd1igICiDdDuvVBpAF4pobxwVX"
		}})
		.done(function(data) {
			killSpinner();
			$.each(data.facets, function(i) {
				var mname = data.facets[i].name;
				$merchantHolder.append("<div merchantname=\"" + mname +  "\" class=\"merchantName clickable\">" + mname + "</div>");
			});
			$('.merchantName').click(function(e) {listItems(e);});
		});
};

listItems = function(e) {
	merchantName = $(e.target).attr("merchantname");
	var appDiv = $(".appwrap");
	appDiv.empty();
	var wcpcList = [];
	wcpcCounts = {};
	appDiv.append("<div id=\"itemHeader\" class=\"header\">" + merchantName + "</div>");
	addSpinner(appDiv);
	
	// Get individual products
	var $header = $('#itemHeader');
	$.ajax({
		// wcpc instead of module
		//url: "https://insights-api.newrelic.com/v1/accounts/366519/query?nrql=SELECT%20uniqueCount(dsply_uuid)%20from%20Transaction%20where%20profile%3D%27webcollage%27%20and%20module%3D%27" + merchantName + "%27%20facet%20mpn%20since%201%20week%20ago%0A", 
		url: "https://insights-api.newrelic.com/v1/accounts/366519/query?nrql=SELECT%20uniqueCount(dsply_uuid)%20from%20Transaction%20where%20profile%3D%27webcollage%27%20and%20" + storeType + "%3D%27" + merchantName + "%27%20facet%20wcpc%20since%201%20week%20ago%20limit%20" + uniLimit, 
		headers: {
			"Accept": "application/json",
			"X-Query-Key": "S6VfFMLd1igICiDdDuvVBpAF4pobxwVX"
		}})
		.done(function(data) {
			$.each(data.facets, function(i) {
				var wcpc = data.facets[i].name;
				wcpcList.push(wcpc);
				wcpcCounts[wcpc] = data.facets[i].results[0].uniqueCount;
				// Going to sort keys
			});
			sortable = []
			for (var v in wcpcCounts) {
				sortable.push([v, wcpcCounts[v]]);
			}
			var sorted = sortable.sort(function(a, b) {return b[1] - a[1]});
			var maxCount = sorted[0][1];

			/*keysSorted = Object.keys(list).sort(function(a,b){return list[a]-list[b]})*/
			var res = getNamesFromWCPC(wcpcList);
			killSpinner();
			var nameObj = {};
			$.each(res.facets, function(i) {
				nameObj[res.facets[i].name] = res.facets[i].results[0].members[0];
			});
			for (var i = 0; i < sorted.length; i++) {
				var wcpc = sorted[i][0];
				var itemName = nameObj[wcpc];
				var weighted = Math.round((wcpcCounts[wcpc] / maxCount)*100);
				$header.append("<div itemname=\"" + itemName.toString().replace('"', '') + "\" wcpc=\"" + wcpc +  "\" class=\"item clickable\"><span class=\"leftItem\">" + notTooLong(nameObj[wcpc]) + "</span><span class=\"rightItem\">" + weighted + "</span></div>");
			}
			$('.item').click(function(e) {showPP(e);});
		});
};

notTooLong = function(s) {
	var l = 40;
	if (s.length > l) {
		return (s.substring(0, l) + "...");
	} else {
		return s
	}
}


showPP = function(e) {
	var mname;
	var wcpc = $(e.currentTarget).attr("wcpc");
	if (merchantName == "") {
		mname = $(e.currentTarget).attr("merchantname");
	} else {
		if (storeType == "partnerid") {
			mname = getModuleFromPartner(merchantName, wcpc);
		} else {
			mname = merchantName;
		}
	}
	console.log(e.currentTarget);
	console.log(wcpc);

	var appDiv = $(".appwrap");
	appDiv.empty();
	appDiv.append("<div class=\"header\">" + $(e.currentTarget).attr("itemname") +  "</div>");
	//appDiv.append("<div id=\"header\" class=\"header\">" + wcpc + "</div>");
	appDiv.append("<div id=\"wc-power-page\"></div> ");
	var wcDiv = $("#wc-power-page");
	addSpinner(wcDiv);
    appDiv.append(" <script type=\"text/javascript\">" + 
			" ;(function (g) { " + 
		" var d = document, i, am = d.createElement('script'), h = d.head || d.getElementsByTagName(\"head\")[0], " + 
  " aex = {\"src\": \"http://gateway.answerscloud.com/innovation-demo/production/gateway.min.js\", \"type\": \"text/javascript\", \"async\": \"true\", \"data-vendor\": \"acs\",\"data-role\": \"gateway\"}; " + 
  " for (var attr in aex) { am.setAttribute(attr,aex[attr]); } " + 
  " h.appendChild(am); " + 
  " g['acsReady'] = function () {var aT = '__acsReady__', args = Array.prototype.slice.call(arguments, 0),k = setInterval(function () {if (typeof g[aT] === 'function') {clearInterval(k);for (i = 0; i < args.length; i++) {g[aT].call(g, function(fn) { return function() { setTimeout(fn, 1) };}(args[i]));}}}, 50);}; " + 
" })(window); " + 
			" acsReady(function(){ " + 
				" Webcollage.loadContent('innovation-demo', \"" + mname + "-" + wcpc + "\"); " + 
			" }); " + 
" </script> ");
	othersLiked(appDiv, mname, wcpc);
};

othersLiked = function(appDiv, mname, wcpc) {
	// Get individual products
	$.ajax({
		url: "https://insights-api.newrelic.com/v1/accounts/366519/query?nrql=SELECT%20count(dsply_uuid),%20bloom(dsply_uuid)%20%20FROM%20Transaction%20where%20wcpc%3D%27" + wcpc + "%27",
		headers: {
			"Accept": "application/json",
			"X-Query-Key": "S6VfFMLd1igICiDdDuvVBpAF4pobxwVX"
		}})
	.done(function(data) {
		if (data.results[0].count == 0) {
			return false;
		} else {
			getRetailerList(appDiv, wcpc);
			getBloomers(appDiv, data);
		}
	});

};

getBloomers = function(appDiv, d) {
	var wcpcList = [];

	$.ajax({
		type: "POST",
		url: "https://insights-api.newrelic.com/v1/accounts/366519/query",
		data: "nrql=SELECT%20count(*),%20uniques(mpn),%20uniques(module)%20FROM%20Transaction%20where%20bloom(dsply_uuid,%27" + encodeURIComponent(d.results[1].serializedFilter) + "%27)%20facet%20wcpc%20since%201%20week%20ago limit 15",
		headers: {
			"Accept": "application/json",
			"X-Query-Key": "S6VfFMLd1igICiDdDuvVBpAF4pobxwVX"
		}})
	.done(function(data) {
		appDiv.append("<div id=\"othersHeader\" class=\"header\">People who viewed this also viewed:</div>");
		var $header = $('#othersHeader');
		$.each(data.facets, function(i) {
			var wcpc = data.facets[i].name;
			var itemName = data.facets[i].results[1].members[0];
			var mname = data.facets[i].results[2].members[0];
			$header.append("<div itemname=\"" + itemName.toString().replace('"', '') + "\" merchantname=\"" + mname +  "\" wcpc=\"" + wcpc +  "\" class=\"item clickable\">" + notTooLong(itemName) + "</div>");
		});

			$('.item').click(function(e) {showPP(e);});
	});
}

getRetailerList = function(appDiv, wcpc) {
	console.log('here');
	$.ajax({
		url: "https://insights-api.newrelic.com/v1/accounts/366519/query?nrql=SELECT%20uniques(referrer)%20FROM%20Transaction%20where%20wcpc%3D%27" + wcpc + "%27%20facet%20partnerid%20since%201%20week%20ago",
		headers: {
			"Accept": "application/json",
			"X-Query-Key": "S6VfFMLd1igICiDdDuvVBpAF4pobxwVX"
		}})
	.done(function(data) {
		console.log('result');
		console.log(data);
		appDiv.append("<div id=\"buyHeader\" class=\"header\">Buy this product here:</div>");
		var $header = $('#buyHeader');

		$.each(data.facets, function(i) {
			var name = data.facets[i].name;
			var url = data.facets[i].results[0].members[0];
			$header.append("<div class=\"lout\"><a class=\"linkout\" onclick=\"window.open(this.href,'_system'); return false;\" href=\"http://go.redirectingat.com?id=24079X830152&xs=1&url=" + url + "\">" + name + "</a></div>");
		});

			/*$('.item').click(function(e) {showPP(e);});*/
	});
}


getModuleFromPartner = function(merch, wcpc) {
	var yo = null;
	$.ajax({
		// wcpc instead of module
		url: "https://insights-api.newrelic.com/v1/accounts/366519/query?nrql=SELECT%20uniques(module)%20FROM%20Transaction%20where%20partnerid%3D%27" + merch + "%27%20and%20wcpc%20%3D%20%27" + wcpc+ "%27%20since%201%20week%20ago",
		async: false,
		headers: {
			"Accept": "application/json",
			"X-Query-Key": "S6VfFMLd1igICiDdDuvVBpAF4pobxwVX"
		}})
		.done(function(data) {
			yo = data.results[0].members[0]

		});
	return yo;
}

getNamesFromWCPC = function(wcpcList) {
	var yo = null;
	var smString = "";
	if (storeType == null) {
		smString = "%20" + storeType + "%3D%27" + merchantName + "%27%20and"
	}
	$.ajax({
		//url: "https://insights-api.newrelic.com/v1/accounts/366519/query?nrql=SELECT%20uniques(module)%20FROM%20Transaction%20where%20partnerid%3D%27" + merch + "%27%20and%20wcpc%20%3D%20%27" + wcpc+ "%27%20since%201%20week%20ago",
		url: "https://insights-api.newrelic.com/v1/accounts/366519/query?nrql=SELECT%20uniques(mpn)%20FROM%20Transaction%20where%20" + smString + "%20wcpc%20in%20(%27" + wcpcList.join("%27,%27") + "%27)%20facet%20wcpc%20%20since%201%20week%20ago%20limit%20" + uniLimit,
		async: false,
		headers: {
			"Accept": "application/json",
			"X-Query-Key": "S6VfFMLd1igICiDdDuvVBpAF4pobxwVX"
		}})
		.done(function(data) {
			yo = data;
		});
	return yo;
}

app.initialize();
